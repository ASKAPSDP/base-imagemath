/// @file PrimaryBeam.h
///
/// @abstract
/// Base class for primary beams
/// @ details
/// defines the interface to the Primary Beam structures for the purpose of image
/// based weighting or (via an illumination) the gridding.
///

#ifndef ASKAP_PRIMARYBEAM_H

#define ASKAP_PRIMARYBEAM_H

#include <Common/ParameterSet.h>
#include <boost/shared_ptr.hpp>

#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/IPosition.h>
#include <casacore/casa/BasicSL/Complex.h>

namespace askap
{
    namespace imagemath
    {

        class PrimaryBeam {

        public:

            /// Shared pointer definition
            typedef boost::shared_ptr<PrimaryBeam> ShPtr;

            PrimaryBeam();
            virtual ~PrimaryBeam();

            PrimaryBeam(const PrimaryBeam &other);

            /// This has to be static as we need to access it in the register even
            /// if there is no instantiated class.
            static ShPtr createPrimaryBeam(const LOFAR::ParameterSet& parset);

            virtual double evaluateAtOffset(double offset, double frequency) = 0;

            virtual double evaluateAtOffset(double offsetPA, double offsetDist, double frequency) = 0;
            virtual void evaluateAlphaBetaAtOffset(float& alpha, float& beta,
                                                   double offsetPA, double offsetDist, double frequency) = 0;
            virtual double getLeakageAtOffset(double offsetPAngle, double offsetDist, int pol, double frequency) = 0;

            virtual casacore::Matrix<casacore::Complex> getJonesAtOffset(double offset, double frequency) = 0;

            /// @brief obtain Jones matrix
            /// @details Sample primary beam model in a given direction.
            /// Definition of direction coords is dependent on inheriting class
            /// @param[in] dir1 1st direction coord in radians: ra (SKA_LOW_PB), az (MWA_PB)
            /// @param[in] dir2 2nd direction coord in radians: dec (SKA_LOW_PB), za (MWA_PB)
            /// @param[in] freq frequency in Hz
            virtual casacore::Matrix<casacore::Complex> getJones(double dir1, double dir2, double freq) = 0;

        private:

        }; // class
    } // imagemath
} //askap

#endif
